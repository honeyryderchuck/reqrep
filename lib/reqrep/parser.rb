module ReqRep
  module Parser
    extend self

		def [](mimetype)
      parsers[mimetype]
    end
    # maps mime/type to parser
    def parsers
      @parsers ||= {}
    end

    def register(mimetype, parser)
      parsers[mimetype] = parser
    end


    require "reqrep/parser/form"
    require "reqrep/parser/multipart"
    register "application/x-www-form-urlencoded", Parser::Form
    register "multipart/form-data", Parser::Multipart
  end
end
