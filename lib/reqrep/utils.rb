module ReqRep::Utils
  extend self

  def parse_media_type(content_type, default: "text/plain")
    return [default, {}] if content_type.nil?
    media_type, *props = content_type.split(%r/\s*[;,]\s*/)
    props = Hash[props.map { |prop| 
      k, v = prop.split("=" ,2)
      v = ((v.start_with?("?") && v.end_with?("?")) ||
           (v.start_with?("\"") && v.end_with?("\""))) ? v[1..-2] : v
      [k.downcase, v] 
    }]

    [media_type || default, props]
  end
end
