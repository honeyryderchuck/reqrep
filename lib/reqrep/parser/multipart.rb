module ReqRep::Parser
  module Multipart
    extend self

    def parse(payload, boundary: , **options)
      parser = ReqRep::Multipart::Form.new(boundary)
      parser.parse(payload)
      parser
    end
  end
end
