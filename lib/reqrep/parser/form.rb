module ReqRep::Parser
  module Form
    extend self

    def parse(payload, encoding: Encoding::UTF_8, **options) #each
      URI.decode_www_form(payload, encoding, **options).reduce({}, &method(:reduce_to_array))
    end


    def reduce_to_array(values, (k, v))
      if v.is_a?(Array)
        v = v.reduce({}, &method(__method__))
      end
      if values[k]
        values[k] = (Array(values[k]) << v)
      else
        values[k] = v
      end
      values
    end
  end
end
