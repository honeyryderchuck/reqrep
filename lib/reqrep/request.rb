module ReqRep
  module Request
    def host
      headers.get(":authority") ||
      headers.get("host") ||
      uri.select(:host, :port).compact.join(":")
    end

    def content_length
      headers.get("content-length") || -1
    end

    # @return [URI] uri as an object
    def uri
      URI.parse(url)
    end

    def post_form(**options)
      return {} if body.empty?
      parser = Parser[media_type]
      raise "don't know how to parse media type #{media_type}" unless parser

      parser.parse(body.join, encoding: media_charset, **options)
    end

    def multipart_form
      post_form(boundary: media_properties["boundary"])
    end

    def form
      query_form = parse_form || {}
      case media_type
      when "multipart/form-data"
        query_form.merge!(multipart_form)
      else
        query_form.merge!(post_form)
      end
      query_form
    end

    private

    def parse_form
      query = uri.query
      return unless query
      parser = Parser["application/x-www-form-urlencoded"]
      parser.parse(query)
    end

    def media_type
      @media_type || begin
        media_properties
        @media_type
      end
    end

    def media_charset
      @media_charset ||= media_properties["charset"] || "utf-8"
    end

    def media_properties
      @media_properties ||= begin
        @media_type, props = Utils.parse_media_type(headers.get("content-type"))
        props
      end
    end
  end
end
