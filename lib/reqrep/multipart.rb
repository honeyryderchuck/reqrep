require "multipart_parser/parser"
module ReqRep
  module Multipart
    class Part 
      TOKEN = /[^\s()<>,;:\\"\/\[\]?=]+/
      VALUE = /"(?:\\"|[^"])*"|#{TOKEN}/
      CONDISP = /\s*#{TOKEN}\s*/i
      BROKEN_QUOTED = /^#{CONDISP}.*;\sfilename="(.*?)"(?:\s*$|\s*;\s*#{TOKEN}=)/i
      BROKEN_UNQUOTED = /^#{CONDISP}.*;\sfilename=(#{TOKEN})/i
      MULTIPART_NAME = /^.*\s+name=(#{VALUE})/ni
      UNQUOTE_REGEX = /\A"(.*)"\Z/
  
      attr_reader :media_type, :file
      def initialize(headers)
       @disposition = headers["content-disposition"]
       @media_type, @media_properties = Utils.parse_media_type(headers["content-type"],
                                                               default: "application/octet-stream")
       fname = if filename
         [File.basename(filename), File.extname(filename)]
       else
         "multipart"
       end
       @file = headers["file"] || Tempfile.new(fname, encoding: encoding)
      end

      def <<(chunk)
        @file.write(chunk)
      end

      def filename
        @filename ||= begin
          @disposition[BROKEN_QUOTED, 1] ||
          @disposition[BROKEN_UNQUOTED]
        end
      end
  
      def name
        @name ||= begin
          name = @disposition[MULTIPART_NAME, 1]
          if name
            name[UNQUOTE_REGEX, 1] || name # try to unquote
          else
            filename
          end
        end
      end

      private

      def encoding
        Encoding.find(@media_properties["charset"] || (@media_type == "text/plain" ? "utf-8" : "binary"))
      end
    end

    class Form
  
      attr_reader :file 
  
      def initialize(boundary)
       @values = []
       @headers = {}
       @data = String.new
       @finished = false
       @parser = MultipartParser::Parser.new
       @parser.init_with_boundary(boundary)
       @parser.on(:part_begin, &method(:part_begin))
       @parser.on(:header_field, &method(:header_field))
       @parser.on(:header_value, &method(:header_value))
       @parser.on(:header_end, &method(:header_end))
       @parser.on(:part_data, &method(:data))
       @parser.on(:part_end, &method(:data_end))
       @parser.on(:end, &method(:parse_end))
      end
 
      def parse(reader)
        reader = case reader
        when String then StringIO.new(reader)
        else
          raise "can't read input for multipart" unless reader.respond_to?(:each)
          reader
        end
        ea = reader.each
        self << ea.next until finished?
      rescue StopIteration
        raise EOFError, "bad multipart body"
      ensure
        reader.close
      end
 
      def <<(chunk)
        @parser.write(chunk)
      end
  
      def finished? ; @finished ; end
 
      def [](k)
        @values[k] if @values.is_a?(Hash)
      end

      def to_hash
        @values
      end
 
      private  
      def part_begin(*)
        @header_field = String.new
        @header_value = String.new
      end
  
      def header_field(b, st, en)
        @header_field << b[st...en].downcase
      end
  
      def header_value(b, st, en)
        @header_value << b[st...en]
      end
  
      def header_end(*)
        @headers[@header_field] = @header_value
        @header_field = String.new
        @header_value = String.new
      end
 
      def data(b, st, en)
        part_file << b[st...en]
      end
  
      def data_end(*)
        part_file.file.rewind
        @values << file
        reset_part_file
      end

      def parse_end(*)
        @finished = true
        @values = Hash[@values.map {|file|
          [file.name, file] 
        }]

      end

      def part_file
        @file ||= begin
          file = Part.new(@headers)
          @headers.clear
          file
        end
      end

      def reset_part_file
        @file = nil
      end
    end
  end
end
