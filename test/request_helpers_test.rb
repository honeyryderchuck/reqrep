require_relative "test_helper"
require "reqrep/request"

class ReqRepRequest < Minitest::Test

  def test_verb
    get  = RequestMock.new(verb: "GET")
    post = RequestMock.new(verb: "POST")

    assert get.verb == "GET", "get verb must be GET"
    assert post.verb == "POST", "get verb must be POST"
  end

  def test_uri
    req = RequestMock.new(url: "/")
    uri = req.uri

    assert uri.path  == "/", "request path is unexpected"
    assert uri.query == nil, "request query is unexpected"

    req2 = RequestMock.new(url: "/admin?id=1")
    uri2 = req2.uri

    assert uri2.path  == "/admin", "request path is unexpected"
    assert uri2.query == "id=1", "request query is unexpected"
  end

  def test_content_length
    req = RequestMock.new
    assert req.content_length == -1, "content length is unexpected"

    req2 = RequestMock.new(headers: {"content-length" => 5})
    assert req2.content_length == 5, "content length is unexpected"
  end

  def test_transfer_encoding

  end

  def test_host
    req = RequestMock.new(headers: {"host" => "jaguar.com:8080"})
    assert req.host == "jaguar.com:8080", "host is unexpected"

    req2 = RequestMock.new(headers: {":authority" => "jaguar.com:8080" })
    assert req2.host == "jaguar.com:8080", "host is unexpected"

    req3 = RequestMock.new(url: "https://jaguar.com:8080/admin/?id=1")
    assert req3.host == "jaguar.com:8080", "host is unexpected"

    req4 = RequestMock.new(headers: {":authority" => "jabberwocky.com" }, 
                           url: "https://jaguar.com:8080/admin/?id=1")
    assert req4.host == "jabberwocky.com", "host is unexpected"
  end

  def test_form
    req0 = RequestMock.new
    assert req0.form.respond_to?(:[]), "form is not a key-value map"
    assert req0.form.empty?

    req1 = RequestMock.new(url: "/admin?id=1")
    assert req1.form.respond_to?(:[]), "form is not a key-value map"
    assert req1.form["id"] == "1"

    req2 = RequestMock.new(headers: {"content-type" => "application/x-www-form-urlencoded"},
                          body: "foo=bar&arr=too&arr=legit")
    assert req2.post_form.respond_to?(:[]), "form is not a key-value map"
    assert req2.post_form["foo"] == "bar"
    assert req2.post_form["arr"] == %w(too legit)

    req3 = RequestMock.new(url: "/admin?id=1",
                          headers: {"content-type" => "application/x-www-form-urlencoded"},
                          body: "foo=bar&arr=too&arr=legit")
    assert req3.post_form.respond_to?(:[]), "form is not a key-value map"
    assert req3.form["id"] == "1"
    assert req3.form["foo"] == "bar"
    assert req3.form["arr"] == %w(too legit)

    req4 = RequestMock.new(url: "/admin?id=1",
                          headers: {"content-type" => "multipart/form-data, boundary=123aef"},
                          body: multipart_body)
    assert req4.form.respond_to?(:[]), "form is not a key-value map"
    assert req4.form["id"] == "1"
    assert req4.form["upload"].is_a?(ReqRep::Multipart::File)

  end

  def test_post_form
    req1 = RequestMock.new(headers: {"content-type" => "application/x-www-form-urlencoded"})
    assert req1.post_form.respond_to?(:[]), "post form doesn't respond to #[]"
    assert req1.post_form.empty?, "post form should be empty for empty bodies"

    req2 = RequestMock.new(headers: {"content-type" => "application/x-www-form-urlencoded"},
                          body: "foo=bar&arr=too&arr=legit")
    assert req2.post_form.respond_to?(:[]), "post form doesn't respond to #[]"
    assert req2.post_form["foo"] == "bar"
    assert req2.post_form["arr"] == %w(too legit)
  end

  def test_multipart_form
    req1 = RequestMock.new(headers: {"content-type" => "multipart/form-data, boundary=123aef"},
                          body: multipart_body)
    assert req1.multipart_form.is_a?(ReqRep::Multipart::Form), "not a multipart file"
    assert !req1.multipart_form["upload"].nil?
    assert req1.multipart_form["upload"].filename == "reqrep.txt"
  end

  def trailer

  end

  def remote_addr

  end

  def request_uri
    req = RequestMock.new(url: "https://google.com/q=cats")
    assert req.is_a?(URI), "req is not a uri type"
    asssert req.request_uri == "https://googe.com/q=cats", "request uri is unexpected"
  end

  def tls

  end

  private

  def request
    @request ||= RequestMock.new
  end

  def multipart_body 
    "--123aef\r\n" +
    "Content-Disposition: form-data; name=\"upload\"; filename=\"reqrep.txt\"\r\n" +
    "Content-Type: text/plain\r\n\r\n" +
    "Wiribam" +
    "\r\n--123aef--\r\n"
  end

end



