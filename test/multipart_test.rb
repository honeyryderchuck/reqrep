require_relative "test_helper"
require "reqrep/multipart"

# mostly adapted from rack's multipart tests
class MultipartFormTest < Minitest::Test

  def multipart_parse(name, boundary: "123aef")
    file = multipart_file(name)
    ReqRep::Parser::Multipart.parse(file, boundary: boundary)
  end

  def multipart_file(name)
    File.new(File.join(__dir__, "fixtures", "multipart", name))
  end

  def test_parse_single_body
    multipart = multipart_parse("singlebody")

    assert multipart.finished?, "parsing should have finished"
    assert multipart["upload"].filename == "reqrep.txt"
    assert multipart["upload"].media_type == "text/plain"
    assert !multipart["upload"].file.nil?, "file hasn't been buffered"
    file = multipart["upload"].file
    # file is properly named if name is available
    assert File.extname(file.path) == ".txt"
    text = file.read
    assert text == "Wiribam", "file has unexpected content"
    assert text.encoding == Encoding::UTF_8
  end

  def test_parse_multiple_files

    multipart = multipart_parse("multibody")

    assert multipart["upload1"].filename == "upload1.txt"
    assert multipart["upload1"].media_type == "text/plain"
    assert !multipart["upload1"].file.nil?, "file hasn't been buffered"
    file = multipart["upload1"].file
    assert file.read == "Wiribam1", "file has unexpected content"
    
    assert multipart["upload2"].filename == "upload2.txt"
    assert multipart["upload2"].media_type == "text/plain"
    assert !multipart["upload2"].file.nil?, "file hasn't been buffered"
    file = multipart["upload2"].file
    assert file.read == "Wiribam2", "file has unexpected content"

    assert multipart["upload3"].filename == "upload3.txt"
    assert multipart["upload3"].media_type == "text/plain"
    assert !multipart["upload3"].file.nil?, "file hasn't been buffered"
    file = multipart["upload3"].file
    assert file.read == "Wiribam3", "file has unexpected content"

  end

  def test_no_filename
    multipart = multipart_parse("nofilename")
    assert multipart["text"].filename.nil?, "filename shouldn't be defined"
    text = multipart["text"].file.read
    assert text.encoding == Encoding::US_ASCII
  end

  def test_no_defined_content_type
    multipart = multipart_parse("none")
    assert multipart["submit-name"].media_type == "application/octet-stream"
    text = multipart["submit-name"].file.read
    assert text.encoding == Encoding::BINARY
  end

  def test_quoted_encoding
    multipart = multipart_parse("quotedencoding")
    assert multipart["user-sid"].mime_type == "text/plain"
    text = multipart["user-sid"].file.read
    assert text.encoding == Encoding::UTF_8 
  end

  def test_webkit
    multipart = multipart_parse("webkit", boundary: "----WebKitFormBoundaryWLHCs9qmcJJoyjKR")
    assert multipart["profile"].has_key?("bio")
    assert multipart["profile"].has_key?("public_email")
    assert multipart["profile"]["bio"].include?("hello")
  end

  #def test_length_mismatch
  #  assert_raises(EOFError) do
  #    multipart = multipart_parse("empty")
  #  end
  #end

  def test_no_name
    multipart = multipart_parse("filenameandnoname")
    assert !multipart["file1.txt"].nil?
    assert multipart["file1.txt"].filename == "file1.txt"
  end

  def test_name_with_semicolons
    multipart = multipart_parse("semicolons")
    assert !multipart["files"].nil?
    assert multipart["files"].filename == "fi;le1.txt"
  end

  def test_quoted_boundary
    multipart = multipart_parse("quotedboundary", boundary: "123:aef")
    assert !multipart["submit-name"].nil?
    text = multipart["submit-name"].file.read
    assert text == "Larry"
    assert !multipart["submit-name-with-content"].nil?
    text = multipart["submit-name-with-content"].file.read
    assert text == "Berry"
  end
#https://tools.ietf.org/html/rfc7578
#
#Unicodes over there
#--AaB03x
#content-disposition: form-data; name="field1"
#content-type: text/plain;charset=UTF-8
#content-transfer-encoding: quoted-printable
#
#Joe owes =E2=82=AC100.
#--AaB03x
#
#First part defines charset
#--AaB03x
#content-disposition: form-data; name="_charset_"
#
#iso-8859-1
#--AaB03x--
#content-disposition: form-data; name="field1"
#
#...text encoded in iso-8859-1 ...
#AaB03x--
#
#4.7 Content Transfer Encoding is deprecated, move on
#
#ASCII-only param names
#
#https://tools.ietf.org/html/rfc7578#section-5.1.2

end

